pipeline {
  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: docker
            image: docker:latest
            command:
            - cat
            tty: true
            volumeMounts:
             - mountPath: /var/run/docker.sock
               name: docker-sock
          volumes:
          - name: docker-sock
            hostPath:
              path: /var/run/docker.sock    
        '''
    }
  }
  environment {
        KUBECONFIG = credentials('k3s-config')
  }
  stages {

    stage('Clone') {    
      steps {
        script { 
          if (env.gitlabBranch.startsWith("refs/tags/")) {
                        env.gitTag = env.gitlabBranch[10..env.gitlabBranch.length()-1]
                        echo "${env.gitTag}"
                    }
        }
        container('docker') {
          git([url: 'https://gitlab.com/alexvarko/Landing-Page-CI.git', branch: 'master', credentialsId: 'Gitlab_for_Jenkins'])
        }
      }
    }
    stage('Build-Docker-Image') {
      steps {       
        container('docker') {        
          script {
            if(env.gitTag) {
              echo "Tag: ${env.gitTag}"
              sh "docker build -t registry.gitlab.com/alexvarko/landing-page-ci:${env.gitTag} ."
            } else if(env.gitlabBranch!="master"){
              echo "Commit_Hash: ${env.GIT_COMMIT}"
              sh "docker build -t registry.gitlab.com/alexvarko/landing-page-ci:${env.GIT_COMMIT} ."
            } else{
              echo "Nothing to build"
            }          
            }
        }
      }
    }
    stage('Login-Into-Docker') {
      steps {
        container('docker') {
          withCredentials([usernamePassword(credentialsId: '02e7dedf-efd4-46a4-8c9f-db20496aa7fd', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                        sh 'docker login registry.gitlab.com -u $DOCKER_USERNAME -p $DOCKER_PASSWORD'
                    }
      }
    }
    }
     stage('Push-Images-Docker-to-GitlabRegistry') {
      steps {
        container('docker') { 
          script {
            if(env.gitTag) {
              sh "docker push registry.gitlab.com/alexvarko/landing-page-ci:${env.gitTag}"
            } else if(env.gitlabBranch!="master"){
              echo "Commit_Hash: ${env.GIT_COMMIT}"
              sh "docker push registry.gitlab.com/alexvarko/landing-page-ci:${env.GIT_COMMIT}"
            } else{
              echo "Nothing to push"
            }          
            }
      }
    }
     }
    stage('Deploy Helm chart') {
            steps {
              container('docker') { 
                script {
                  sh 'wget https://get.helm.sh/helm-v3.12.2-linux-amd64.tar.gz'
                  sh 'ls -a'
                  sh 'tar -xvzf helm-v3.12.2-linux-amd64.tar.gz'
                  sh 'cp linux-amd64/helm /usr/bin'
                  sh 'helm version'
            if(env.gitTag) {
              sh "helm upgrade --install landing -f 'landing-chart/values.yaml' --create-namespace --namespace=production landing-chart --set image.repository=registry.gitlab.com/alexvarko/landing-page-ci,image.tag=${env.gitTag},ingress.hosts[0].host='147.182.195.139.sslip.io',ingress.tls[0].hosts[0]='147.182.195.139.sslip.io'"
            } else if(env.gitlabBranch!="master"){
              echo "Commit_Hash: ${env.GIT_COMMIT}"
              sh "helm upgrade --install landing -f 'landing-chart/values.yaml' --create-namespace --namespace=staging landing-chart --set image.repository=registry.gitlab.com/alexvarko/landing-page-ci,image.tag=${GIT_COMMIT}"
            } else{
              echo "Nothing to deploy"
            }          
            }
            }
        }
    }
  }
  

    post {
      always {
        container('docker') {
          sh 'docker logout'
      }
      }
    }
}

